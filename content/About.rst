About
=====

:date: 20200804 08:30
:tags: welcome
:category: Intro
:slug: welcome
:author: William Salmon
:summary: Welcome document

A few things about me: 
 * I like engineering
 * I did a aerospace engineering degree
 * I used to be a electromagnet's engineer
 * I am a software `engineer`


I am very dyslexic so this blog will definitely not be a shining paragon of grammatical virtue and I am not to fussed about spelling ether. I just want a place to share some of the funny things that I have learn and that I am happy for others to copy.

Some of it will just be little more than a note to self for things that others might also find useful.

