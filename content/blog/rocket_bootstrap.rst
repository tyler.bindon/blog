Bootstrap Rocket
================

:date: 20200822 08:30
:tags: rust, web, rocket
:category: rust
:slug: Rocket-Diesel-Bootstrap
:author: William Salmon
:summary: Setting up rocket

Rocket Overview
---------------

Rocket is a flask like framework for rust.

Here are my notes for running rocket on fedora


Rocket State
------------

Diesel looks like a sensible way to work with data bases with rust.

To install diesels cli on fedor you need some headers:


        sudo dnf install community-mysql-libs


So you can then build it with:


        cargo install diesel_cli --no-default-features --features "mysql sqlite"


Then you can follow something like:

http://diesel.rs/guides/getting-started/

Personal notes on Diesel:
  * Its pretty snazzy
  * I wish it was more Db agnostic
  * I wish i didnt have to create my own migrations
  * Due to the previous two points i think using sqlite locally and something else
    in prod will be a bit tricky, i suspect i will end up using docker more than i
    want for this project.

Using Diesel in Rocket
----------------------

once you have deisel setup we can move to Rocket


You should then  then do something like https://dzone.com/articles/creating-a-rest-api-in-rust-using-rocket-and-diese
or https://medium.com/sean3z/building-a-restful-crud-api-with-rust-1867308352d8
but these do not play well with the diesel in the previous section.

I did not follow the previous links as i wanted to follow the newer diesel pattens 

For instance they depend directly on r2d2 rather than getting them directly from rocket or diesel.

Using Diesel and Rocket together
--------------------------------

A tutorial like https://cprimozic.net/blog/rust-rocket-cloud-run/ actually runs..

This is nice and recent and easy to follow

And all runs

A very basic example can be found https://gitlab.com/pointswaves/points-that-need-doing/-/commit/c982ca4be45b4bcb3aaa9655404755259ad1820b

Next steps
----------


I have skimed over the details till now as i have just been trying to follow tutorials

I will now look for tutorial for the following and maybe make one were i cannot find good resource.

Todo:
    * Web pages:
        * Wrap some html, i think rocket has templates to avoid too much boiler plate.
        * Use forms not just curl and json
        * Need some basic static css so things look nice with minimal effort
    * Basic docker fore easy deployment
   

Post
----

This blog also uses the older direct data base access rather than using the rocket_contrib but it also has lots of good stuff
like examples of post data etc.

https://lankydan.dev/2018/05/20/creating-a-rusty-rocket-fuelled-with-diesel
