Pcb Arrival
===========

:date: 20181210 08:30
:tags: PointBlank, PCB, electronics
:category: PointBlank
:slug: pcb-arrives
:author: William Salmon
:summary: Pointblank pcb Arrives





The board arrived some time over the weekend.

.. image:: media/electronics/2018Dec_pcb_first_pick.jpg
   :width: 200pt
   :alt: unpopulated pcb


Now comes the task of populating it!

After having made small bugs with other boards, I have found that for a new board the best policy it to slowly populate the first one not in the order in which it is easiest to solder it but to prove each "section" of the design.

eg. Solder up the power sections first, then get the micro to work. Then as each subsequent section gets populated, you can check it did not upset the running of others.

However this is only possible as I am hand soldering a relatively simple board and that the different "sections" do not really interact with each other but rather each section connects to the micro with it able to operate with only a section of the other areas working.




