New PCB
=======

:date: 20181119 08:30
:tags: PointBlank, PCB, electronics
:category: PointBlank
:slug: new-pcb
:author: William Salmon
:summary: Work on a Pointblank pcb



Intro
-----

I really like electronics and as I used to be a engineer, but now a software developer, it's a nice excuse to use skills that I really enjoy and value but rarely get to use any more.

My friend Alex and some of his friends from work, collaborated on a number of RobotWars robots. For the latest one I helped out with some general engineering and some electrical engineering.

Doug, one of Alex's friends/colleagues, and myself created a number of control boards for the robot Point Blank.


The Board
---------

The board takes simple input from a remote control radio like you would find on any other hobbyist quad-copter or remote control car. It then turns it into instructions for the values that actuate the pneumatic weapon on the robot.

This is my 3rd or 4th iteration of the weapon board and the second one that I have had printed from my own design.

This is the most complex board I have designed and it has the most components that I will have soldered in one go. In preparation I created a mini PCB that contained the most difficult to solder parts, so I hope I should be able to solder the components well.

The main sections of the board are:
   * radio ports
   * low power output, 8 ports
   * micro controler
   * analog in, 4 ports
   * USB/Power management
   * high current outputs, 4 ports.

.. image:: media/electronics/2018Nov_weapon_front.png
   :width: 200pt
   :alt: pcb front

.. image:: media/electronics/2018Nov_weapon_back.png
   :width: 200pt
   :alt: pcb back

There are many aspects of the board I am not yet satisfied with but I have put in quite a lot of effort over a long time to improve these. I have created test circuits for most of the systems, however I think more progress will be made from having the board printed than fettling the design.


