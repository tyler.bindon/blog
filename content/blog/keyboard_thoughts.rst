Keyboard Thoughts
=================

:date: 20200804 08:30
:tags: Keyboard, PCB, electronics
:category: keyboard
:slug: keyboard-thoughts
:author: William Salmon
:summary: Thinking about keyboards




I really like the idea of the Ergodox but I like doing my own thing so I have long fancied the idea of making my own.

.. image:: media/keyboard/keyboardPCB.png
   :width: 200pt
   :alt: pcb design for keyboard

As you can see a log of the design is taken up with a teensy 3.5 as with `my most recent pcb <{filename}new_pcb.rst>`_.

I really like the teensy but i dont like how hard it is to integrate the usb with the board. I have been thinking about programming a micro with a different tool chain and with my own circuit around a cortex-M4 but at the time i created that PCB design i did not have a better solution than the teensy.

Earlier this year i learned some rust and really liked using it.

Rust has a interesting tool chain for Cortext-M4 chips and especially good stm32 support so i have started looking in to these chips as they should be able to act as a usb keyboard and be hand solder-able and easy to program so i am going to put some effort in to learning how to use the stm32s with rust with the longer turm goal of designing my own pcb and even longer term goal of making a keyboard and not being tied to using a teensy like dev board.
