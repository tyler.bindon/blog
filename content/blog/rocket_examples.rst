Combining Rocket building blocks
================================

:date: 20200910 08:30
:tags: rust, web, rocket
:category: rust
:slug: Rocket-Diesel-Examples
:author: William Salmon
:summary: Using Rocket

Rocket Intro
------------


My last `post <rocket_bootstrap.rst>`_ I introduced rocket and linked to some nice intro tutorials I found.


Since then i have been developing a `todo app <https://gitlab.com/pointswaves/points-that-need-doing/>`_

This is a reasonably basic example but more complex than some of the others that I have seen so far.

This post will not aim to repeat how each basic element works but instead try to give examples of how they can be used together.

Rust type system can be quite ridge but i have been really impressed at how well the different parts needed for this app fit together.

Databases -- Diesel
-------------------

I have found the `QueryDSL Doc <https://docs.diesel.rs/diesel/query_dsl/trait.QueryDsl.html>`_ to be the most useful page for working out how to go beond the basics

Often database querys are built up, Django lets you build up filter()'s but only exicutes the SQL once you atually ask for the data

Django

.. code-block:: python

        Query = django_data_model.objects.filter(name_eq="bob")
                .filter(age_eq=25)
                .sort_by(coolness)

This would tell django how to build up a select statement but not database calls would be called until

.. code-block:: python

        data_from_db = Query.all()

You can use diesel in a similar way

.. code-block:: rust

        let all_data = tasks::dsl::tasks.filter(tasks::parent.is_null())
                .filter(tasks::done.is_null())
                .load::<(Task, Option<Category>)>(&conn.0)
                .map_err(|err| -> Template {
                        println!("Error getting todo items from the database: {:?}", err);
                        Template::render("errors", &TemplateError{
                                error_detail: format!("Error getting todo items from the database: {:?}", err),
                                error_type: "Database Error:".into(),
                        })
                }
        )?;

Ignoring the map_err bit this is very similar but determining the type returned by the filter can be a bit difficult which makes i hard to deal with the query

Determining type can be the tricyist problem but ownership is also a bit interesting as each filter() consume the previous QueryDSL and returns a new QueryDSL.

The aforementioned `QueryDSL Doc <https://docs.diesel.rs/diesel/query_dsl/trait.QueryDsl.html>`_ exmplains this much better than I and also give a nice sulution. If you need to hold
on to and programaticly tweak these querys at run rather than compile time then you can box them and then modifiy them


This then lets you 

.. code-block:: rust

        let mut query = join.filter(tasks::parent.eq(this_task.id)).into_boxed();
        if let ShowItems::Todo = show_done {
                query = query.filter(tasks::done.is_null());
        }
        let all_data = query.load::<(Task, Option<Category>)>(&conn.0).map_err(|err| -> Template {
                println!("Error getting todo items from the database: {:?}", err);
                Template::render("errors", &TemplateError{
                        error_detail: format!("Error getting todo items from the database: {:?}", err),
                        error_type: "Database Error:".into(),
                })
        })?;

Nice Error pages
----------------

In the examples above the errors are of Template type, this means that not only can we make nice normal pages we can also have nice error pages putting this all together gives

.. code-block:: rust

        #[get("/task")]
        pub fn task_all(conn: DbConn) -> Result<Template, Template> {
            let join = tasks::table.left_join(categories::table);
            let all_data = join.filter(tasks::parent.is_null())
                .filter(tasks::done.is_null())
                .load::<(Task, Option<Category>)>(&conn.0).map_err(|err| -> Template {
                    println!("Error getting todo items from the database: {:?}", err);
                    Template::render("errors", &TemplateError{
                            error_detail: format!("Error getting todo items from the database: {:?}", err),
                            error_type: "Database Error:".into(),
                        })
                }
            )?;
            let todo_items = recurce_task_list(&conn, all_data, &ShowItems::Todo)?;
            let template_data = TemplateData{todo_list: todo_items};
            println!("{:?}", template_data);
            Ok(Template::render("list_todo", &template_data))
        }

Our standard 404, 500 etc error messages can also be styled to match the rest of our ShowItems

First we create a custom catch

.. code-block:: rust

        #[catch(404)]
        pub fn not_found(
                req: &Request,
        ) -> Template {
                let logged_in_user = LogedInUser::from_request(req);
                let logged_in = match logged_in_user {
                        rocket::Outcome::Success(_) => true,
                        _ => false,
                };
                let mut context: HashMap<&str, &str> = HashMap::new();
                context.insert("error_type", "Page not Found: 404");
                context.insert("error_detail", "We could not find this page for you");
                if logged_in {
                        context.insert("logged_in", "True");
                }
                Template::render("error_404", context)
        }

Note that the catch dose not support cookie based gards but we can still use them
by making them from the request ourselves. In this case our nav bar displays different
options for logged in vs non logged in users. And we can keep this functionality even
in our error pages.

Then we add it to our Rocket

.. code-block:: rust

        rocket::ignite()
                .mount(
                "/",
                routes![
                        routes_site::index,
                ],
                )
                .attach(DbConn::fairing())
                .attach(cors::CorsFairing)
                .attach(Template::fairing())
                .register(catchers![routes_site::not_found])
                .launch();




Forms and databases
-------------------

In order to take advantage of rusts great type system you do need to define a lot of types and structs, some times you
can reuse these types.

But some times you do need to convert from one type to anther. eg, the TodoForm struct used by rust to validate the data coming from post or query data. the TodoForm also contains lots of info needed to 
create the NewTask struct used to populate the database. But the NewTask also needs some extra data, like the time and the rendered markup.

.. code-block:: rust

        #[derive(Debug, FromForm, Deserialize)]
        pub struct TodoForm {
                name: String,
                description: String,
                category: Option<i32>,
                parent: Option<i32>,
                group: i32,
        }

        impl TodoForm {
                pub fn to_task(&self) -> NewTask {
                        let html: String = markdown::to_html(&self.description.as_str());
                        NewTask {
                                name: &self.name,
                                discription: &self.description,
                                discription_cache: html,
                                created: SystemTime::now()
                                        .duration_since(SystemTime::UNIX_EPOCH)
                                        .unwrap()
                                        .as_secs() as i32,
                                category: self.category,
                                parent: self.parent,
                                todogroup: self.group,
                        }
                }
        }

Using a public `to_*` function can implement the faily boilerplate like code converting one type to anther, this function could have been done with the From trait
for even more stream liked code.

The resulting add_todo function is consise and still readable:

.. code-block:: rust

        #[post("/add_todo", data = "<todo_data>")]
        pub fn add_todo(
                conn: DbConn,
                todo_data: Result<Form<TodoForm>, FormError>,
                _logged_in_user: LogedInUser,
        ) -> Result<Redirect, WebErrors> {
                let form: Form<TodoForm> = todo_data?;
                let new_task = form.to_task();

                use crate::diesel::Connection;
                use diesel::result::Error;
                let _inserted_tasks: std::vec::Vec<Task> = conn.0.transaction::<_, Error, _>(|| {
                        let inserted_count = diesel::insert_into(schema::tasks::dsl::tasks)
                        .values(&new_task)
                        .execute(&conn.0)?;

                        Ok(schema::tasks::dsl::tasks
                        .order(schema::tasks::dsl::id.desc())
                        .limit(inserted_count as i64)
                        .load(&conn.0)?
                        .into_iter()
                        .rev()
                        .collect::<Vec<_>>())
                })?;
                Ok(Redirect::to("/"))
        }


Joins
-----

Diesel tries to help you with joins, and can do most of the work for simple joins.

The following SQL will allow deisel to update your schema file :

.. code-block:: SQL

        CREATE TABLE tasks (
                id INTEGER NOT NULL PRIMARY KEY,
                name VARCHAR NOT NULL,
                discription TEXT NOT NULL,
                discription_cache TEXT NOT NULL,
                created INT NOT NULL,
                started INT,
                due INT,
                category INT,
                parent INT,
                done INT,
                todogroup INT NOT NULL,
                FOREIGN KEY  (category)  REFERENCES categories (id)
        );

Diesel can tell that tasks and categories may need to be in the same query so will add the following when `diesel_cli` is run.

.. code-block:: rust

        joinable!(tasks -> categories (category));

        allow_tables_to_appear_in_same_query!(categories, group_members, tasks, todogroups, users,);

The above allows use to then create joined structures from the structs for the individule tables that can then be used as normal as follows

.. code-block:: rust

        let join = tasks::table.left_join(categories::table);
        let mut query = join.filter(tasks::parent.eq(this_task.id)).into_boxed();
        if let ShowItems::Todo = show_done {
            query = query.filter(tasks::done.is_null());
        }
        let all_data = query.load::<(Task, Option<Category>)>(&conn.0)?;

Diesel functions that normally return types or vectors of types we have defined in our models.rs simplely ruturn a combination of these types, in this case we
can use a option to represent a foreign key that can be NUL.


Many to Many
------------

Diesel dose not implement any special functions for many to many relation ships but if you create your own joining table between two other tables like points todos table
that joins the users and todogroups:

.. code-block:: SQL

        CREATE TABLE group_members (
                user  INTEGER  NOT NULL,
                todogroup  INTEGER  NOT NULL,
                admin BOOLEAN NOT NULL,
                PRIMARY KEY  (user, todogroup),
                FOREIGN KEY  (user)  REFERENCES  users (id),
                FOREIGN KEY  (todogroup)  REFERENCES  todogroups (id)
        );

Once we have the table to hold the many to many relationships within our database we can then use normal joins to create a qureies to call on the database connection

.. code-block:: rust

    let join_members = schema::group_members::table.inner_join(schema::todogroups::table);
    let join_groups = schema::group_members::table
        .inner_join(schema::todogroups::table)
        .inner_join(schema::users::table);
    let todo_groups: std::vec::Vec<(GroupMember, Todogroup)> = join_members
        .filter(schema::group_members::user.eq(user.user_id))
        .load(&conn.0)?;
    let group_list = todo_groups
        .into_iter()
        .map(|(group_member, todogroup)| {
            let member_list: std::vec::Vec<(GroupMember, Todogroup, User)> = join_groups
                .filter(schema::todogroups::id.eq(todogroup.id))
                .load(&conn.0)
                .unwrap();
            ...
        }

The above code block is quite in volved but i was keen to include some more complex examples in this blog as while the diesel docks cover all of the ways you may
use diesel it dose not always cover the ticks needed to use everything in combination with everything else.

For instance for a many to many relations ship you start with the joining table and inner_join outwards, with the first join able to be joined again.

the best way to use these joined structures may depend on your databases topolagy, for instance, in this case, a user will only be part of a limited number of groups
so having a database call to retreive these groups and then a second call to retreive all of the other useres in those groups is not overly expensive for the 
page that shows all the groups a user is in. espeacally as this page will not need to be viewed too often.

these joins with 3 or more tables are also able to return structures composed of the structs we created in our models.rs files so we do not need to create any more structs
sepcifically for the joined tables.

Queries and functions
---------------------

We will often need to put database interactions in to functions often this is needed as our functions get bigger but this can very 
very helpful to allow for de-duplications or even recusion.

.. code-block:: rust

        fn recurce_task_list(
                conn: &DbConn,
                task_list: Vec<(Task, Option<Category>)>,
                show_done: &ShowItems,
        ) -> Result<Vec<TodoItem>, WebErrors> {

recusively calling to a database can lead to a excesive database calls but some times is helpful.

the above snipit just passes down the database connection and a list of Tasks to recuse over and in turn returns a result for a vector of items for a template.

A more sensible function like `todo_query` in points todo deduplicates a common set of filters that return the top level todo items that a given user has
permision to see and that are still not yet done.

The `todo_query` functions like the `recurce_task_list` function takes a database conection as well as the id of the user to create the query for.
but in thise case the function must return a boxed query for a joined set of tables.

TODO, i actually need to sort this out in code and then write up who todo do it nicely rather then just copyand pasteing from the compiler error and ending up with this horry 
function

.. code-block:: rust

        // There are things in Diesel that should allow this to be "derived"
        // That will make it much more redable, and also robust.
        // If any of the data base tables get updated then this will need updating
        #[allow(clippy::type_complexity)]
        fn todo_query<'a>(
                conn: &DbConn,
                user_id: i32,
        ) -> diesel::query_builder::BoxedSelectStatement<
        'a,
        (
                (
                diesel::sql_types::Integer,
                diesel::sql_types::Text,
                diesel::sql_types::Text,
                diesel::sql_types::Text,
                diesel::sql_types::Integer,
                diesel::sql_types::Nullable<diesel::sql_types::Integer>,
                diesel::sql_types::Nullable<diesel::sql_types::Integer>,
                diesel::sql_types::Nullable<diesel::sql_types::Integer>,
                diesel::sql_types::Nullable<diesel::sql_types::Integer>,
                diesel::sql_types::Nullable<diesel::sql_types::Integer>,
                diesel::sql_types::Integer,
                ),
                diesel::sql_types::Nullable<(
                diesel::sql_types::Integer,
                diesel::sql_types::Text,
                diesel::sql_types::Text,
                )>,
        ),
        diesel::query_source::joins::JoinOn<
                diesel::query_source::joins::Join<
                schema::tasks::table,
                schema::categories::table,
                diesel::query_source::joins::LeftOuter,
                >,
                diesel::expression::operators::Eq<
                diesel::expression::nullable::Nullable<schema::tasks::columns::category>,
                diesel::expression::nullable::Nullable<schema::categories::columns::id>,
                >,
        >,
        diesel::sqlite::Sqlite,
        > {

Even clippy knows the above needs sorting out lol

Daniel pointed me at https://docs.rs/diesel/1.4.5/diesel/helper_types/index.html which should help a lot, once i get round to it.

Custom errors
-------------

Following on form using a template to create nice error pages with a map for every error we will look at using our own error type. This will nicely functionalise
the maps and will also make our primary function much smaller and more readable as well as being more idiomatic rust.

Taking the task function from earlier:

.. code-block:: rust

        #[get("/task")]
        pub fn task_all(conn: DbConn) -> Result<Template, Template> {
            let join = tasks::table.left_join(categories::table);
            let all_data = join.filter(tasks::parent.is_null())
                .filter(tasks::done.is_null())
                .load::<(Task, Option<Category>)>(&conn.0).map_err(|err| -> Template {
                    println!("Error getting todo items from the database: {:?}", err);
                    Template::render("errors", &TemplateError{
                            error_detail: format!("Error getting todo items from the database: {:?}", err),
                            error_type: "Database Error:".into(),
                        })
                }
            )?;
            let todo_items = recurce_task_list(&conn, all_data, &ShowItems::Todo)?;
            let template_data = TemplateData{todo_list: todo_items};
            println!("{:?}", template_data);
            Ok(Template::render("list_todo", &template_data))
        }

By using the `?` operator to 'resolve' the result ruturned from the load function ether to our CustomError or to the expacted value.

.. code-block:: rust

        #[get("/task")]
        pub fn task_all(conn: DbConn) -> Result<Template, CustomError> {
            let join = tasks::table.left_join(categories::table);
            let all_data = join.filter(tasks::parent.is_null())
                .filter(tasks::done.is_null())
                .load::<(Task, Option<Category>)>(&conn.0)?;
            let todo_items = recurce_task_list(&conn, all_data, &ShowItems::Todo)?;
            let template_data = TemplateData{todo_list: todo_items};
            println!("{:?}", template_data);
            Ok(Template::render("list_todo", &template_data))
        }

But for this to work we need our CustomError to be created from the error returned by the load.

And for Result<Template, CustomError> to satisfy the reququirements of the `get`. `get` requires that the funtion returns something that implments the responder trait.
the Rocket lib implement the responder trait for rocket when its ok and err elements implement responder and debug.

.. code-block:: rust

        /// Responds with the wrapped `Responder` in `self`, whether it is `Ok` or
        /// `Err`.
        impl<'r, R: Responder<'r>, E: Responder<'r> + fmt::Debug> Responder<'r> for Result<R, E> {
                fn respond_to(self, req: &Request) -> response::Result<'r> {
                        match self {
                                Ok(responder) => responder.respond_to(req),
                                Err(responder) => responder.respond_to(req),
                        }
                }
        }

As you can see the Results implementation of respnder just calls the respond_to function for the respective versions of its self. We can follow that patten for our own enum

.. code-block:: rust

        impl<'r> Responder<'r> for CustomError {
                fn respond_to(self, request: &Request) -> response::Result<'r> {
                        match self {
                        WebErrors::Form(template)
                        | WebErrors::Serialize(template)
                        | WebErrors::Oxide(template)
                        | WebErrors::Database(template)
                        | WebErrors::Password(template)
                        | WebErrors::UserNotFound(template) => template.respond_to(&request),
                        WebErrors::Misc(error_type, error_detail) => {
                                let temp = Template::render(
                                "errors",
                                &TemplateError {
                                        error_type,
                                        error_detail,
                                },
                                );
                                temp.respond_to(&request)
                        }
                        }
                }
        }

These all use the template but they could use any thing that implements Responder or create there own response::Result

The second part is to implement From for the errors that you wish to handle or generator functions for functions that return ambiguse error types, like `()`

.. code-block:: rust

        impl From<diesel::result::Error> for CustomError {
                fn from(error: diesel::result::Error) -> Self {
                        WebErrors::Database(Template::render(
                        "errors",
                        TemplateError {
                                error_type: "Database error".to_string(),
                                error_detail: format!("Details: {:?}", error),
                        },
                        ))
                }
        }

As you can see this function is very similar to the map function that it replaces but by using this function we only need to implement this once rather than for every map.